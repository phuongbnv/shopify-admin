@extends('layout.index')

@section('title', 'Doasboard')

@section('content')
    <div class="main-content">
        <div class="top-page">
            <div class=" container-scroller">
                <div class="container-fluid page-body-wrapper full-page-wrapper">
                  <div class="main-panel w-100  documentation">
                    <div class="content-wrapper">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-12 pt-5">
                            <a class="btn btn-primary" href="assets/index.html"><i class="ti-home me-2"></i>Back to home</a>
                          </div>
                        </div>
                        <div class="row pt-5 mt-5">
                          <div class="col-12 pt-5 text-center">
                            <i class="text-primary mdi mdi-file-document-box-multiple-outline display-1"></i>
                            <h3 class="text-primary fw-light mt-5">
                              The detailed documentation of Star Admin2  Template is provided at 
                              <a href="http://bootstrapdash.com/demo/star-admin-pro/docs/documentation.html" target="_blank" class="text-danger d-block text-truncate">
                                http://bootstrapdash.com/demo/star-admin-pro/docs/documentation.html
                              </a>
                            </h3>
                            <h4 class="mt-4 fw-light text-primary">
                              In case you want to refer the documentation file, it is available at 
                              <span class="text-danger">/docs/documentation.html</span> 
                              in the downloaded folder
                            </h4>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- partial:../../partials/_footer.html -->
                    <footer class="footer">
                  <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
                  </div>
                </footer>
                    <!-- partial -->
                  </div>
                </div>
              </div>
        </div>
   </div>
@endsection